image:
  name: hashicorp/terraform:0.14.10
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# Default output file for Terraform plan
variables:
  PLAN: plan.tfplan
  PLAN_JSON: tfplan.json
  TF_IN_AUTOMATION: "true"

cache:
  key: "$CI_COMMIT_SHA"
  paths:
    - .terraform

.gitlab-tf-backend: &gitlab-tf-backend
  - export TF_ADDRESS=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/production
  - export TF_HTTP_ADDRESS=${TF_ADDRESS}
  - export TF_HTTP_LOCK_ADDRESS=${TF_ADDRESS}/lock
  - export TF_HTTP_LOCK_METHOD=POST
  - export TF_HTTP_UNLOCK_ADDRESS=${TF_ADDRESS}/lock
  - export TF_HTTP_UNLOCK_METHOD=DELETE
  - export TF_HTTP_USERNAME=gitlab-ci-token
  - export TF_HTTP_PASSWORD=${CI_JOB_TOKEN}
  - export TF_HTTP_RETRY_WAIT_MIN=5
  - echo "Using HTTP Backend at $TF_HTTP_ADDRESS"
  - terraform --version
  - terraform init -reconfigure

.install-jq: &install-jq
  - apk add --update jq
  - alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"

before_script:
  - *gitlab-tf-backend

stages:
  - validate
  - plan
  - apply
  - destroy


validate:
  stage: validate
  script:
    - terraform validate
    - terraform fmt -check
  only:
    - branches
    - merge_requests

merge review:
  stage: plan
  script:
    - *install-jq
    - terraform plan -out=$PLAN
    - "terraform show --json $PLAN | convert_report > $PLAN_JSON"
  artifacts:
    expire_in: 1 week
    name: plan
    paths:
      - $PLAN
    reports:
        terraform: $PLAN_JSON
  only:
    - merge_requests

plan production:
  stage: plan
  script:
    - terraform plan
  only:
    - main
  resource_group: production

apply:
  stage: apply
  script:
    - terraform apply -auto-approve
  dependencies:
    - plan production
  artifacts:
    expire_in: 1 week
    name: $CI_COMMIT_REF_SLUG
  only:
    - main
  resource_group: production
  environment:
    name: production
    on_stop: destroy
    auto_stop_in: 12 hours

destroy:
  stage: destroy
  needs: []
  script:
    - terraform destroy -auto-approve
  when: manual
  only:
    - main  
  environment:
    name: production
    action: stop

